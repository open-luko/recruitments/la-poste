def test_send_letter(client):
    response = client.post("/letters")
    assert response.status_code == 200
    data = response.json
    assert set(data.keys()) == {"id", "la_poste_code", "status"}
    assert data["id"] is not None
    assert data["la_poste_code"] is not None
    assert data["status"] is None