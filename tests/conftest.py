import os
from pytest import fixture

from app import create_app, db as _db


@fixture(scope="session", autouse=True)
def app():
    os.environ["APP_CONFIG"] = "test"
    app = create_app()
    with app.app_context():
        yield app


@fixture(scope="function")
def client(app):
    with app.test_client() as client:
        yield client


@fixture(scope="session", autouse=True)
def db(app):
    """Session-wide test database."""
    _db.create_all()
    yield _db
    _db.drop_all()
