from app import db


class Letter(db.Model):
    __tablename__ = "letter"

    id = db.Column(db.Integer, primary_key=True)
    la_poste_code = db.Column(db.String(256), nullable=False)
    status = db.Column(db.String(256))

    def to_dict(self):
        return {
            "id": self.id,
            "la_poste_code": self.la_poste_code,
            "status": self.status,
        }
