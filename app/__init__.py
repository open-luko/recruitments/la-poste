import os

from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

from app.config import config

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)

    CORS(app, origins="*", supports_credentials=True)

    config_name = os.getenv("APP_CONFIG") or "development"
    app.config.from_object(config[config_name])

    db.init_app(app)

    from .views import api

    app.register_blueprint(api)

    return app
