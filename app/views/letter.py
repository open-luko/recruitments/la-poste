from app import db
from app.mocks import send_letter_mock
from app.views import api
from app.models.letter import Letter


@api.route("/letters", methods=["POST"])
def ep_send_letter():
    """
    Send a letter via La Poste (mocked)
    Get a fake La Poste ID with an empty status.
    """
    # Mocking call to La Poste API to send a letter
    letter_data = send_letter_mock()
    letter = Letter()
    letter.la_poste_code = letter_data["id"]
    db.session.add(letter)
    db.session.commit()
    return letter.to_dict(), 200
