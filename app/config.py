class Config:
    SQLALCHEMY_DATABASE_URI = "sqlite:///app.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestConfig(Config):
    ENV_TYPE = "development"
    SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"


class DevelopmentConfig(Config):
    ENV_TYPE = "development"


config = {
    "development": DevelopmentConfig,
    "test": TestConfig,
}
