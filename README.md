# Case study
# Context

We use La Poste Suivi API to track the letters we send to our customers. However, La Poste API is not always up. So we want to build a back-end service to mirror the API to achieve high availability.

The service already exists and allows to send letters via La Poste API *(mocked since this is a case study).* We need to add the ability to read the letter status after they have been sent. We usually send several thousands of letters per day and we expect the number of customers to increase by 10 times every year for the following years.

# To do

## Requirements

Add to the service the following capabilities:

- For a given letter, return the status stored locally
- For a given letter, fetch the status from LaPoste API and return it
- Update the status of all the letters stored in the DB

The letter status is the code of the latest event in the field `event` (DR1, DR2, EP1, etc). If the letter is not found in La Poste API, the status is `None`.

The endpoints must be tested.

## Bonus requirements

*These are only if you have time/want to impress us. They can also be discussed in the case study debrief.*

- For a given letter, return the history of its statuses
- Improve service quality:
    - Input validation
    - Documentation
    - etc
- Improve code quality
    - Linting
    - Type checking
    - Formatting
    - etc

# Notes

1. The codebase is provided to help you start the case study faster. You are free to modify it as you wish.
2. The service comes with a SQLite DB pre-filled with a table `letter` that contains 5 letters available in La Poste Suivi API sandbox environment and 1000 letters that are not in La Poste API. The codes of the 5 letters of the sandbox environment are:
    - LU680211095FR
    - LZ712917377US
    - 8K00009775862
    - 861011301731382
    - CB662173705US 
3. You can run the script `insert_mock_data_in_db.py` to insert as many letters as you want in the DB (to test performance with a high volume for example).
4. La Poste API documentation is here: https://developer.laposte.fr/products/suivi/2
5. There's a sandbox key for La Poste Suivi API in the `.env` file. You do not need to create an account on La Poste website.

# How to develop

## Setup

- Install python 3.9 & pipenv
- `pipenv install` to install the project's requirements

## Run

- `pipenv shell` to enter the virtual environment (loading the variables in `.env`)
- `flask run`
